using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using keremet.Data;
using keremet.Models;
using keremet.Models.ProductModels;
using keremet.Models.AccountViewModels;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authorization;
namespace keremet.Controllers
{
  public class ProductController : Controller
  {
    // Declares the DbContext class
    ApplicationDbContext _ctx;
    // The instance of DbContext is passed via dependency injection
    public ProductController(ApplicationDbContext ctx)
    {
      _ctx=ctx;
    }
    // GET: /<controller>/
    // Return the list of cars to the caller view
    public IActionResult Index()
    {
      return View(_ctx.Products.ToList());
    }
    public IActionResult Create()
    {
      return View();
    }
    // Add a new object via a POST request
    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Create(Product p)
    {
      // If the data model is in a valid state ...
      if (ModelState.IsValid)
      {
        // ... add the new object to the collection
        _ctx.Products.Add(p);
        // Save changes and return to the Index method
        _ctx.SaveChanges();
        return RedirectToAction("Index");
      }
      return View(p);
    }
    [ActionName("Delete")]
    public IActionResult Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }
      Product p = _ctx.Products.Single(m => m.Id == id);
      if (p == null)
      {
        return NotFound();
      }
      return View(p);
    }
    // POST: Cars/Delete/5
    // Delete an object via a POST request
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public IActionResult DeleteConfirmed(int id)
    {
      Product p = _ctx.Products.SingleOrDefault(m => m.Id == id);
      // Remove the car from the collection and save changes
      _ctx.Products.Remove(p);
      _ctx.SaveChanges();
      return RedirectToAction("Index");
    }
  }
}