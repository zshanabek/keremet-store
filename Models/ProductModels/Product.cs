using System;
using System.Collections.Generic;
namespace keremet.Models.ProductModels
{
    public class Product
    {
        public int Id {get;set;}
        public string Gender {get;set;}
        public string Type {get;set;}
        public int Size {get;set;}
        public int Model {get;set;}
        public string Color {get;set;}
    }
}